const GdYandexMap = {
    myMap: null,
    
    init(listDeliveryPoint = {}, arPoints = [55.76, 37.64]) {
        this.myMap = new ymaps.Map('gd_ya_map', {
            center: arPoints,
            zoom: 10
        }, {
            searchControlProvider: 'yandex#search'
        });
        let objectManager = new ymaps.ObjectManager({
            clusterize: true,
            gridSize: 32,
            clusterDisableClickZoom: true
        });

        objectManager.objects.options.set('preset', 'islands#blackCircleDotIcon');
        objectManager.clusters.options.set('preset', 'islands#blackClusterIcons');
        this.myMap.geoObjects.add(objectManager);
        objectManager.add(listDeliveryPoint);
    },
};