<?php

$arForMaps['type'] = 'FeatureCollection';
$arForMaps['features'] = [];
foreach ($arResult['ITEMS'] as $k => $arItem) {
    $arForMaps['features'][] = [
        'type' => 'Feature',
        'id' => $k,
        'geometry' => [
            'type' => 'Point',
            'coordinates' => explode(",", $arItem['PROPERTY_COOR_VALUE']),
        ],
        'properties' => [
            'balloonContentHeader' => $arItem['NAME'] . ' (' . $arItem['PROPERTY_CITY_VALUE'] . ')',
            'balloonContentBody' => $arItem['PROPERTY_PHONE_VALUE'],
            'balloonContentFooter' => $arItem['PROPERTY_EMAIL_VALUE'],
        ],
    ];
}
$arResult['LIST_POINTS'] = json_encode($arForMaps, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
