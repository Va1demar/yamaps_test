<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Bitrix\Main\Data\Cache,
    Bitrix\Main\Page\Asset,
    Bitrix\Iblock\IblockTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

class GdYandexMap extends CBitrixComponent
{
    private $_request;

    /**
     * Проверка наличия модулей требуемых для работы компонента
     * @return bool
     * @throws Exception
     */
    private function _checkModules()
    {
        if (!Loader::includeModule('iblock')) {
            throw new \Exception('Не загружены модули необходимые для работы компонента');
        }

        return true;
    }

    public function onPrepareComponentParams($arParams)
    {
        $arParams['IBLOCK_ID'] = (int)$arParams['IBLOCK_ID'] > 0 ? (int)$arParams['IBLOCK_ID'] : 0;
        $arParams['CACHE_TIME'] = (int)$arParams['CACHE_TIME'] > 0 ? (int)$arParams['CACHE_TIME'] : 0;
        $arParams['NEED_PROPS'] = is_array($arParams['NEED_PROPS']) ? $arParams['NEED_PROPS'] : [];
        $arParams['CONNECT_API_MAPS'] = isset($arParams['CONNECT_API_MAPS']) && $arParams['CONNECT_API_MAPS'] === 'Y' ? 'Y' : 'N';

        return $arParams;
    }

    public function executeComponent()
    {
        $this->_checkModules();

        $this->_request = Application::getInstance()->getContext()->getRequest();

        if ($this->arParams['IBLOCK_ID'] > 0) {
            $this->getData();
            if (!empty($this->arResult)) {
                if ($this->arParams['CONNECT_API_MAPS'] === 'Y') {
                    CJSCore::Init(["jquery"]);
                    Asset::getInstance()->addJs('https://api-maps.yandex.ru/2.1/?lang=ru_RU');
                }
                $this->includeComponentTemplate();
            }
        }
    }

    private function getData(): void
    {
        $cache = Cache::createInstance();
        $taggedCache = Application::getInstance()->getTaggedCache();

        $cachePath = '/yandex_map/';
        $cacheTtl = $this->arParams['CACHE_TIME'];
        $cacheKey = 'yandex_map';

        if ($cache->initCache($cacheTtl, $cacheKey, $cachePath)) {
            $this->arResult['ITEMS'] = $cache->getVars();
        } elseif ($cache->startDataCache()) {
            $taggedCache->startTagCache($cachePath);
            $arRes = [];
            if ($this->checkD7()) {
                $className = $this->getApiIblock();
                if (!empty($className)) {
                    $arRes = $this->getDataD7($className);
                } else {
                    $arRes = $this->getDataOld();
                }
            } else {
                $arRes = $this->getDataOld();
            }
            $this->arResult['ITEMS'] = $arRes;
            $taggedCache->registerTag('iblock_id_' . $this->arParams['IBLOCK_ID']);
            $taggedCache->endTagCache();
            $cache->endDataCache($arRes);
        }
    }

    private function getDataOld(): array
    {
        $arRes = [];
        $arOrder = ['SORT' => 'ASC', 'ID' => 'DESC'];
        $arSelect = array_merge(['ID', 'NAME'], $this->convertArSelectOld());
        $arFilter = ['IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'ACTIVE_DATE' => 'Y', 'ACTIVE' => 'Y'];
        $res = CIBlockElement::GetList($arOrder, $arFilter, false, [], $arSelect);
        while ($ob = $res->Fetch()) {
            $arRes[] = $ob;
        }
        return $arRes;
    }

    private function getDataD7($className): array
    {
        $class = 'Bitrix\Iblock\Elements\Element' . $className . 'Table';
        $arRes = $class::getList([
            'order' => ['SORT' => 'ASC', 'ID' => 'DESC'],
            'select' => array_merge(['ID', 'NAME'], $this->convertArSelectD7()),
            'filter' => ['ACTIVE' => 'Y'],
        ])->fetchAll();
        return $arRes;
    }

    private function convertArSelectOld(): array
    {
        $arRes = [];
        foreach ($this->arParams['NEED_PROPS'] as $prop) {
            $arRes[] = 'PROPERTY_' . $prop;
        }
        return $arRes;
    }

    private function convertArSelectD7(): array
    {
        $arRes = [];
        foreach ($this->arParams['NEED_PROPS'] as $prop) {
            $arRes['PROPERTY_' . $prop . '_'] = $prop;
        }
        return $arRes;
    }

    private function checkD7(): bool
    {
        if ($iblock = CModule::CreateModuleObject('iblock')) {
            $needVersion = '20.5.0';
            if (CheckVersion($needVersion, $iblock->MODULE_VERSION)) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    private function getApiIblock(): string
    {
        $arIblock = IblockTable::getById($this->arParams['IBLOCK_ID'])->fetch();
        if (!empty($arIblock['API_CODE'])) {
            return $arIblock['API_CODE'];
        } else {
            return '';
        }
    }
}
