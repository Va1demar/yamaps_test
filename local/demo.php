<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>

<?
if (\Bitrix\Main\Loader::includeModule('iblock')) {
    $arIblock = \Bitrix\Iblock\IblockTable::getList([
        'select' => ['ID'],
        'filter' => [
            'IBLOCK_TYPE_ID' => 'content',
            'CODE' => 'address_for_yandex'
        ],
    ])->fetch();
}
?>

<? $APPLICATION->IncludeComponent(
    "gd:yandex.map",
    "",
    [
        'IBLOCK_ID' => $arIblock['ID'] ?: 0,
        'NEED_PROPS' => [
            'PHONE',
            'EMAIL',
            'COOR',
            'CITY',
        ],
        'CACHE_TIME' => 86400,
        'CONNECT_API_MAPS' => 'Y'
    ],
    false
); ?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>