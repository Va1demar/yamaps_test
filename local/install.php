<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Application;

require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

Loader::includeModule('iblock');

// создание типа ИБ
$typeIB = 'content';

$arFieldsTypeIB = [
    'ID' => 'content',
    'SECTIONS' => 'Y',
    'IN_RSS' => 'N',
    'SORT' => 500,
    'LANG' => [
        'en' => [
            'NAME' => 'Content',
            'SECTION_NAME' => 'Sectione',
            'ELEMENT_NAME' => 'Element'
        ],
        'ru' => [
            'NAME' => 'Контент',
            'SECTION_NAME' => 'Раздел',
            'ELEMENT_NAME' => 'Элемент'
        ]
    ]
];

$obBlocktype = new CIBlockType;
$res = $obBlocktype->Add($arFieldsTypeIB);


// создание ИБ
$context = Application::getInstance()->getContext();
$siteId = $context->getSite();

$ib = new CIBlock;

// Настройка доступа
$arAccess = [
    '2' => 'R',
];

$arFieldsIblock = [
    'ACTIVE' => 'Y',
    'NAME' => 'Адреса',
    'CODE' => 'address_for_yandex',
    'IBLOCK_TYPE_ID' => $typeIB,
    'SITE_ID' => $siteId,
    'SORT' => '500',
    'GROUP_ID' => $arAccess,
    'VERSION' => 1,
];

if ($iblock = CModule::CreateModuleObject('iblock')) {
    $needVersion = '20.5.0';
    if (CheckVersion($needVersion, $iblock->MODULE_VERSION)) {
        echo 'Версия главного модуля [' . $iblock->MODULE_VERSION . '] меньше ' . $needVersion . '<br />';
    } else {
        $arFieldsIblock['API_CODE'] = 'addressya';
        echo 'Версия главного модуля [' . $iblock->MODULE_VERSION . '] больше ' . $needVersion . '<br />';
    }
}


$IBLOCK_ID = $ib->Add($arFieldsIblock);
if ($IBLOCK_ID > 0) {
    echo 'Инфоблок успешно создан<br />';
} else {
    die('Ошибка создания инфоблока<br />');
}

// Создаём свойства
$ibp = new CIBlockProperty;

$arNeedProps = [
    'PHONE' => 'Телефон',
    'EMAIL' => 'Email',
    'COOR' => 'Координаты',
    'CITY' => 'Город',
];

foreach ($arNeedProps as $code => $name) {
    $arFields = [
        'NAME' => $name,
        'ACTIVE' => 'Y',
        'SORT' => 500,
        'CODE' => $code,
        'PROPERTY_TYPE' => 'S',
        'ROW_COUNT' => 1,
        'COL_COUNT' => 70,
        'IBLOCK_ID' => $IBLOCK_ID,
    ];
    $propId = $ibp->Add($arFields);
    if ($propId > 0) {
        echo 'Добавлено свойство ' . $name . ' <br />';
    } else {
        echo 'Ошибка добавления свойства ' . $name . '<br />';
    }
}


// Демоданные для заполнения
$arDemo = [
    [
        'NAME' => 'ЦО в Тамбове',
        'PHONE' => '+7 (800) 555-35-35',
        'EMAIL' => 'tambov@mail.ru',
        'COOR' => '52.721986, 41.453083',
        'CITY' => 'Тамбов',
    ],
    [
        'NAME' => 'На Курской',
        'PHONE' => '+7 (800) 333-53-53',
        'EMAIL' => 'kurskaya@mail.ru',
        'COOR' => '55.758858, 37.656317',
        'CITY' => 'Москва',
    ],
    [
        'NAME' => 'Раменки',
        'PHONE' => '+7 (495) 414-49-78',
        'EMAIL' => 'ramenki@mail.ru',
        'COOR' => '55.697478, 37.500326',
        'CITY' => 'Москва',
    ],
    [
        'NAME' => 'Молодёжная',
        'PHONE' => '+7 (800) 555-55-50',
        'EMAIL' => 'young@mail.ru',
        'COOR' => '55.741065, 37.415488',
        'CITY' => 'Москва',
    ],
    [
        'NAME' => 'Планерная',
        'PHONE' => '+7 (495) 662-44-50',
        'EMAIL' => 'planet@mail.ru',
        'COOR' => '55.862425, 37.435033',
        'CITY' => 'Москва',
    ],
    [
        'NAME' => 'Свиблово',
        'PHONE' => '+7 (800) 100-24-24',
        'EMAIL' => 'sviblovo@mail.ru',
        'COOR' => '55.854997, 37.653388',
        'CITY' => 'Москва',
    ],
];

foreach ($arDemo as $Item) {
    $el = new CIBlockElement;

    $PROP = [];
    $PROP['PHONE'] = $Item['PHONE'];
    $PROP['EMAIL'] = $Item['EMAIL'];
    $PROP['COOR'] = $Item['COOR'];
    $PROP['CITY'] = $Item['CITY'];

    $arLoadProductArray = array(
        'IBLOCK_SECTION_ID' => false,
        'IBLOCK_ID' => $IBLOCK_ID,
        'PROPERTY_VALUES' => $PROP,
        'NAME' => $Item['NAME'],
        'ACTIVE' => 'Y',
    );

    if ($PRODUCT_ID = $el->Add($arLoadProductArray))
        echo 'Создан элемент : ' . $PRODUCT_ID . '<br />';
    else
        echo 'Ошибка: ' . $el->LAST_ERROR . '<br />';
}
